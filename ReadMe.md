<h1><strong>Django Torchvision App</strong></h1>

1. User uploads an image.
2. Send image data to the backend for inference.
3. Return back relevant data as a response [Only Labels or (Labels + New Image)]

<br>

### **Notes**

- Cannot be delpoyed on heroku due to slug size restrictions.