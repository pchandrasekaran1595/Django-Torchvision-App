import json

from django.shortcuts import render
from django.http import JsonResponse

from static.utils import CFG, decode_image, encode_image

def infer(request):
    if request.method == "POST":
        JSONData = request.POST.get("data")

        imageData = json.loads(JSONData)["imageData"]
        infer_type = json.loads(JSONData)["infer_type"]

        cfg = CFG(infer_type)
        header, image = decode_image(imageData)
        disp_image = image.copy()
        h, w, _ = disp_image.shape
        detected_labels = cfg.infer(image, disp_image, w, h)

        encodedImageData = encode_image(header, disp_image)
        
        return JsonResponse({
            "imageData" : encodedImageData,
            "label" : detected_labels,
        })

    return render(request=request, template_name="infer_det/index.html", context={})
