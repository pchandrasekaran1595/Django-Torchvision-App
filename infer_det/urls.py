from django.urls import path
from . import views


app_name = "infer_det"


urlpatterns = [
    path('', views.infer, name="infer_det")
]