from django.apps import AppConfig


class InferDetConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'infer_det'
