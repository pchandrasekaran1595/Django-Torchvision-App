from django.urls import path
from . import views


app_name = "infer_seg"


urlpatterns = [
    path('', views.infer, name="infer_seg")
]