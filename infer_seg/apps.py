from django.apps import AppConfig


class InferSegConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'infer_seg'
