main = () => {
    let image_input = document.querySelector("#image_input")

    let canvas = document.querySelector("canvas")
    let ctx = canvas.getContext("2d")
    let w = canvas.getAttribute("width")
    let h = canvas.getAttribute("height")

    let image = new Image()
    let imageData = null

    let classify = document.querySelector("#classify")
    let reset = document.querySelector("#reset")
    let output = document.querySelector("#output")
    
    image_input.addEventListener("change", (e1) => {
        if(e1.target.files){
            let imageFile = e1.target.files[0]
            let reader = new FileReader()
            reader.readAsDataURL(imageFile)
            reader.onload = (e2) => {
                image.src = e2.target.result
                image.onload = () => {
                    ctx.drawImage(image, 0, 0, w, h)
                    imageData = canvas.toDataURL("image/jpeg", 0.92)
                }
            }
        }
    })

    classify.addEventListener("click", () => {
        let data = {
            data : JSON.stringify({
                imageData : imageData,
                infer_type : "classify",
            }),
        }
        
        const csrftoken = Cookies.get("csrftoken")

        $.ajax({
            type : "POST",
            url : "",
            headers : {
                "X-CSRFToken" : csrftoken
            },
            data : data,
            timeout : 2000,
            success : (response) => {
                console.log(" ---------- ")
                console.log("Success")
                console.log("Status Text : " + response["statusText"])
                console.log(" ---------- ")

                output.value = response["label"]
            },
            error : (response) => {
                console.log(" ---------- ")
                console.log("Failure")
                console.log("Status Text : " + response["statusText"])
                console.log(" ---------- ")
            }
        })
    })

    reset.addEventListener("click", () => {
        ctx.clearRect(0, 0, w, h)
        image.src = ""
        image_input.value = ""
        output.value = ""
    })
    
}

main()