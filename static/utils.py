import io
import re
import cv2
import json
import base64
import numpy as np

from PIL import Image
from torch import argmax
from torchvision import models, transforms, ops

import warnings
warnings.filterwarnings("ignore")

#####################################################################################################

def decode_image(imageData) -> tuple:
    header = imageData.split(",")[0]
    imageData = imageData.split(",")[1]
    image = np.array(Image.open(io.BytesIO(base64.b64decode(imageData))))
    image = cv2.cvtColor(src=image, code=cv2.COLOR_BGRA2RGB)
    return header, image

#####################################################################################################

def encode_image(header, image) -> str:
    _, imageData = cv2.imencode("image.jpeg", image)
    imageData = base64.b64encode(imageData)
    imageData = str(imageData).replace("b'", "").replace("'", "")
    imageData = header + "," + imageData
    return imageData

#####################################################################################################

def segmenter_decode(class_index_image: np.ndarray) -> np.ndarray:
    colors = np.array([(0, 0, 0), (128, 0, 0), (0, 128, 0), (128, 128, 0), (0, 0, 128), (128, 0, 128),
                       (0, 128, 128), (128, 128, 128), (64, 0, 0), (192, 0, 0), (64, 128, 0),
                       (192, 128, 0), (64, 0, 128), (192, 0, 128), (64, 128, 128), (192, 128, 128),
                       (0, 64, 0), (128, 64, 0), (0, 192, 0), (128, 192, 0), (0, 64, 128)])

    r, g, b = np.zeros(class_index_image.shape, dtype=np.uint8), \
              np.zeros(class_index_image.shape, dtype=np.uint8), \
              np.zeros(class_index_image.shape, dtype=np.uint8)

    for i in range(21):
        indexes = (class_index_image == i)
        r[indexes] = colors[i][0]
        g[indexes] = colors[i][1]
        b[indexes] = colors[i][2]
    return np.stack([r, g, b], axis=2)

#####################################################################################################

class CFG(object):
    def __init__(self, infer_type: str):
        self.infer_type = infer_type

        if re.match(r"^classify$", self.infer_type, re.IGNORECASE):
            self.model = models.mobilenet_v3_small(pretrained=True, progress=True).eval()
            for params in self.model.parameters():
                params.requires_grad = False
            self.transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
            self.labels = json.load(open("static/labels_cls.json", "r"))
            self.size = 224
        
        elif re.match(r"^detect$", self.infer_type, re.IGNORECASE):
            self.model = models.detection.fasterrcnn_mobilenet_v3_large_320_fpn(pretrained=True, progress=True).eval()
            for params in self.model.parameters():
                params.requires_grad = False
            self.transform = transforms.Compose([transforms.ToTensor(),])
            self.labels = json.load(open("static/labels_det.json", "r"))
            self.size = 320
        
        elif re.match(r"^segment$", self.infer_type, re.IGNORECASE):
            self.model = models.segmentation.deeplabv3_mobilenet_v3_large(pretrained=True, progress=True).eval()
            for params in self.model.parameters():
                params.requires_grad = False
            self.transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
            self.labels = json.load(open("static/labels_seg.json", "r"))
            self.size = 520
        

    def infer(self, image, disp_image, w, h):
        if re.match(r"^classify$", self.infer_type, re.IGNORECASE):
            if w < self.size and h >= self.size:
                image = cv2.resize(src=image, dsize=(self.size, h), interpolation=cv2.INTER_AREA)
            elif w >= self.size and h < self.size:
                image = cv2.resize(src=image, dsize=(w, self.size), interpolation=cv2.INTER_AREA)
            elif w < self.size and h < self.size:
                image = cv2.resize(src=image, dsize=(self.size, self.size), interpolation=cv2.INTER_AREA)

            output = np.argmax(self.model(self.transform(image).unsqueeze(dim=0)).detach().cpu().numpy())
            return self.labels[str(output)].split(",")[0].title()
        

        elif re.match(r"^detect$", self.infer_type, re.IGNORECASE):
            detected_labels = []
            image = cv2.resize(src=image, dsize=(self.size, self.size), interpolation=cv2.INTER_AREA)
            output = self.model(self.transform(image).unsqueeze(dim=0))

            boxes, labels, scores = output[0]["boxes"], output[0]["labels"], output[0]["scores"]

            if len(boxes) != 0:
                boxes = ops.clip_boxes_to_image(boxes, (w, h))
                # Make this value a user input
                nms_boxes_indices = ops.nms(boxes, scores, 0.8)
                for index in nms_boxes_indices:
                    # Make this value a user input
                    if scores[index] > 0.225:
                        x1, y1, x2, y2 = int(boxes[index][0] / self.size * w), \
                                         int(boxes[index][1] / self.size * h), \
                                         int(boxes[index][2] / self.size * w), \
                                         int(boxes[index][3] / self.size * h)
                        cv2.rectangle(img=disp_image, pt1=(x1, y1), pt2=(x2, y2), color=(0, 255, 0), thickness=2)
                        label = self.labels[str(labels[index].item())].title()
                        detected_labels.append(label)
                        cv2.putText(img=disp_image, text=label, org=(x1-10, y1-10), 
                                    fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 255, 0),
                                    thickness=1)
            else:
                cv2.putText(img=disp_image, text="--- No Objects Detected ---", org=(25, 25), 
                            fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(255, 255, 255),
                            thickness=2)
            
            return list(set(detected_labels))
        
        elif re.match(r"^segment$", self.infer_type, re.IGNORECASE):
            detected_labels = []
            image = cv2.resize(src=image, dsize=(self.size, self.size), interpolation=cv2.INTER_AREA)
            output = self.model(self.transform(image).unsqueeze(dim=0))["out"]
            
            class_index_image = argmax(output[0], dim=0)
            disp_image = cv2.resize(src=segmenter_decode(class_index_image), dsize=(w, h), interpolation=cv2.INTER_AREA)

            class_indexes = np.unique(class_index_image.detach().cpu().numpy())
            for index in class_indexes:
                detected_labels.append(self.labels[str(index)].title())
            return disp_image, detected_labels
        
#####################################################################################################
