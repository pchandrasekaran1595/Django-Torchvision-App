from django.urls import path
from . import views


app_name = "infer_cls"


urlpatterns = [
    path('', views.infer, name="infer_cls")
]