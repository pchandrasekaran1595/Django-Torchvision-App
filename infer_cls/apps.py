from django.apps import AppConfig


class InferClsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'infer_cls'
